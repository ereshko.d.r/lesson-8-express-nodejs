import TicTacToeLogic from "./game_logic.js";

const pathToCross = {
    normal: "../image/x.svg",
    large: "../image/xxl-x.svg",
};
const pathToZero = {
    normal: "../image/zero.svg",
    large: "../image/xxl-zero.svg",
};

class Game {
    constructor(playerX, playerO) {
        this.playerX = playerX;
        this.playerO = playerO;
        this.gameLogic = new TicTacToeLogic(this.playerX, this.playerO);
        this.currentPlayer = this.gameLogic.currentPlayer;
        this.fields = document.getElementsByClassName("cell"); 
        this.#changeCurrentPlayer();    
        this.#initSubjectList(this.playerX, 'first-player');
        this.#initSubjectList(this.playerO, 'second-player');
    };

    #getSymbol() {
        return this.currentPlayer.image.large;
    };

    #putSymbol(numOfField) {
        const element = document.getElementById(numOfField);
        element.innerHTML = `<img src="${this.#getSymbol()}">`;
        element.style.cursor = "default";
    };

    #fillCell(array, color) {
        for (let index of array) {
            const element = document.getElementById(index);
            element.style.background = color;
        };
    };

    #gameOver() {
        for (let field of this.fields) {
            field.removeEventListener("click", main);
            field.style.cursor = "default";
        };
    };
    
    #changeCurrentPlayer() {
        const field = document.getElementById("game-step");
        const name = `${this.currentPlayer.lastName} ${this.currentPlayer.firstName}`;
        const image = this.currentPlayer.image.normal;
        field.innerHTML = `Ходит&nbsp;<img src="${image}" width="24px">&nbsp;${name}`;
    };

    #initSubjectList(player, elementId){
        const playerField = document.getElementById(elementId);
        playerField.children[0].children[0].setAttribute("src", player.image.normal);
        playerField.children[1].children[0].innerHTML = `${player.lastName} ${player.firstName} ${player.surName}`;
        playerField.children[1].children[1].innerHTML = `${player.winRate}% побед`;
    };

    initFieldsListeners(func) {
        for (let field of this.fields) {
            field.addEventListener("click", func);
        };
    };

    main(event) {
        const numOfField = event.target.id;
        const resultOfMove = this.gameLogic.game(numOfField);
        this.#putSymbol(numOfField);
        if (resultOfMove) {
            switch(resultOfMove.winner) {
                case this.gameLogic.PLAYER_X:
                    this.#fillCell(resultOfMove.combination, "#CFEDE6");
                    break;
                case this.gameLogic.PLAYER_O:
                    this.#fillCell(resultOfMove.combination, "#F3BBD0");
                    break;
                case this.gameLogic.DRAW:
                    break;
            };
            this.#gameOver();
        };
        this.currentPlayer = this.gameLogic.currentPlayer;
        this.#changeCurrentPlayer();
    };
};

/*

init data

*/
const player1 = {
    firstName: "Екатерина",
    lastName: "Плюшкина",
    surName: "Викторовна",
    winRate: 60,
    symbol: "x",
    image: pathToCross,
};
const player2 = {
    firstName: "Владлен",
    lastName: "Пупкин",
    surName: "Игоревич",
    winRate: 70,
    symbol: "o",
    image: pathToZero,
};

const game = new Game(player1, player2);
const main = (event) => {
    const field = document.getElementById(event.target.id);
    game.main(event);
    field.removeEventListener("click", main);
};
game.initFieldsListeners(main);

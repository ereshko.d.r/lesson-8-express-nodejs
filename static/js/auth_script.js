const login = document.getElementById("login");
const password = document.getElementById("password");
const button = document.getElementById("auth-form-submit");
const form = document.getElementById("auth-form");
const errorMessages = document.getElementsByClassName("validation-error-message");


class AuthFormValidator {
    isNotEmptyFields() {
        if (login.value !== '' && password.value !== '') {
            button.removeAttribute("disabled")
        }
        else {
            button.setAttribute("disabled", "")
        }
    }

    notValidData() {
        const errorClass = "validation-error-message-container"
        Array.from(errorMessages).forEach(element => {
            element.removeAttribute("hidden")
        });
        login.className = errorClass
        password.className = errorClass
        password.value = ""
    }

    deleteErrorMessages() {
        Array.from(errorMessages).forEach(element => {
            element.setAttribute("hidden", "")
        })
        login.className = ""
        password.className = ""
    }

    handleEvent(event) {
        switch(event.type) {
            case "input":
                this.deleteErrorMessages()
                if (event.target === login) {
                    login.value = login.value.replace(/([^\w\d\.\_])/g, "");
                }
                this.isNotEmptyFields();
                break;
            case "copy":
                event.clipboardData.setData("text/plain", "");
                event.preventDefault();
                break;
            case "cut":
                event.clipboardData.setData("text/plain", "");
                event.preventDefault();
                break;
            case "submit":
                const options = {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({ "login": login.value, "password": password.value }),
                }
                fetch('/auth', options)
                    .then((response) => {
                        if (response.redirected) {
                            window.location.href = response.url
                        }
                        else if (String(response.status).match(/4\d\d$/g)) {
                            return this.notValidData()
                        }
                    })
                    .catch((error) => {
                        console.log(error)
                    })
                break;
        }
    }
}

const validator = new AuthFormValidator();

login.addEventListener("input", validator);
login.addEventListener("copy", validator);
login.addEventListener("cut", validator);
password.addEventListener("input", validator);
password.addEventListener("copy", validator);
password.addEventListener("cut", validator);
password.addEventListener("submit", validator);
form.addEventListener("submit", validator);
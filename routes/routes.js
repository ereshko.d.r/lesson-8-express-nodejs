const {Router} = require('express');
const bcrypt = require('bcryptjs');
const config = require('config');
const router = Router()


router.get('/auth', async (request, response) => {
    try {
        response.render('pages/auth');
    }
    catch (error) {
        response.status(500).json({message: 'something went wrong', error: error});
    }
})
router.post('/auth', async (request, response) => {
    console.log('-- POST /auth')
    try {
        const {login, password} = request.body
        const user = config.get('testUser')
        if (login !== user.username) {
            return response.status(400).json()
        }
        const isMatch = await bcrypt.compare(password, user.password)
        if (!isMatch) {
            return response.status(400).json()
        }
        return response.redirect('/rating')
        
    }
    catch (error) {
        response.status(500).json({message: 'something went wrong', error: error});
    }
})

router.get('/rating', async (request, response) => {
    try {
        const ratingData = [
            {username: 'Александров Игнат Анатолиевич', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Шевченко Рафаил Михайлович', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Мазайло Трофим Артёмович', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Логинов Остин Данилович', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Борисов Йошка Васильевич', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Соловьёв Ждан Михайлович', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Негода Михаил Эдуардович', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Гордеев Шамиль Леонидович', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Волков Эрик Алексеевич', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Стрелков Филипп Борисович', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
            {username: 'Галкин Феликс Платонович', games: '24534', wins: '9824', looses: '1222', rate: '87%'},
        ]
        response.render('pages/rating', {
            ratingData: ratingData
        })
    }
    catch (error) {
        response.status(500).json({message: 'something went wrong', error: error});
    }
})

module.exports = router
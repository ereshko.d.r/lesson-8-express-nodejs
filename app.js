const express = require('express');
const config = require('config');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();

const PORT = config.get('port') ?? 5010

app.set('views', path.join(__dirname, '/static'));
app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(express.static(path.join(__dirname, './static')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true})); 

app.use('/', require('./routes/routes'));

app.listen(PORT, () => {console.log(`Server has been started on ${PORT} port...`)});
